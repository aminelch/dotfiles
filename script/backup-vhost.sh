#!/bin/bash

#########################################################
# Backup my apache2 virtual hosts
#########################################################
# version: 0.0.1
# author: amine LOUHICHI <alu@wevioo.com>
#########################################################

OUTPUT_DIR="$HOME/backup/vhosts"
APACHE_SITE_AVAILABLE="/etc/apache2/sites-available"
CURRENTDATE="$(date +'%m-%d-%y')"

check_directories() {
    [[ -d "$OUTPUT_DIR" ]] || mkdir -p "$OUTPUT_DIR";
    [[ -d "$APACHE_SITE_AVAILABLE" ]] || exit 1;
}

check_directories
mkdir -p $OUTPUT_DIR/vhosts-$CURRENTDATE
find "$APACHE_SITE_AVAILABLE" -maxdepth 1 -type f -name '*.conf' -exec cp -r "{}" "$OUTPUT_DIR/vhosts-$CURRENTDATE/" \;
